Students, who translate English texts, do exercises and do tests
are very good at translating, doing exercises and doing tests,
 but they have problems with understanding English in real life.
 In real life, nobody waits for your translation. People usually use
 simple English when they speak but they use it fast. You have to
  understand with no translation to your native language. If you
  translate, you cannot be part of communication because you are
  thinking about the language too much. These words are maybe hard
  to read but they are true.
You also have to hear every new word 5 to 10 times
if you want to remember it. That’s why we use the same words
 in one level. If you read and hear the same words again and again,
  you will understand them and remember them. If you know words from
   one level, you can go to a higher level and learn new words. It is
   important to go step by step, and read and listen to words which are
   used in English often. This is what we do with our news. In our short news,
    we use words which are used in English often. Level 1 has the 1000 most
    important words. Level 2 has the 2000 most important words, Level 3 has the
     3000 most important words.
So, if you want to understand English fast and learn fast, read two articles or
 more a day. You can improve your reading and listening quickly when you read easy
 English news. We will help you learn English fast and understand it. When you use this
  website every day, you can learn 3000 words which you need for communication with anybody in English.