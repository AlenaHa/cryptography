package org.elena.decryption;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class Digram {
    private String digramString;
    private int numberOfAppearances;
    private float frequency;

    public String getDigramString() {
        return digramString;
    }

    public void setDigramString(String digramString) {
        this.digramString = digramString;
    }

    public int getNumberOfAppearances() {
        return numberOfAppearances;
    }

    public void setNumberOfAppearances(int numberOfAppearances) {
        this.numberOfAppearances = numberOfAppearances;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
}
