package org.elena.decryption;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class Trigram {
    private String trigramString;
    private int numberOfAppearances;
    private float frequency;

    public String getTrigramString() {
        return trigramString;
    }

    public void setTrigramString(String trigramString) {
        this.trigramString = trigramString;
    }

    public int getNumberOfAppearances() {
        return numberOfAppearances;
    }

    public void setNumberOfAppearances(Integer numberOfAppearances) {
        this.numberOfAppearances = numberOfAppearances;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
}
