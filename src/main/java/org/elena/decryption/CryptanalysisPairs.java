package org.elena.decryption;

/**
 * @author Elena Hardon
 * @date 3/9/17.
 */
public class CryptanalysisPairs {
    public Character character;
    public String bigram;
    public String trigram;

    CryptanalysisPairs(Character character, String bigram, String trigram) {
        this.bigram = bigram;
        this.trigram = trigram;
        this.character = character;
    }
}
