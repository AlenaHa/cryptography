package org.elena.decryption;

import org.elena.encription.CriptoPair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class Decryption {

    private String criptotext;
    private String plaintext;
    private int key;
    private List<CriptoPair> encriptionAlphabet = new ArrayList<>(26);

    Decryption(String criptotextFile, int key) {
        File file = new File(criptotextFile);
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                this.criptotext = scanner.next();
            }
            this.key = key;
            this.plaintext = "";
        } catch (Exception e) {
            e.printStackTrace();
        }

        int charValue = 0;
        for (Character character = 'a'; character <= 'z'; character++) {
            CriptoPair criptoPair = new CriptoPair();
            criptoPair.character = character;
            criptoPair.value = charValue;
            encriptionAlphabet.add(criptoPair);
            charValue++;
        }
    }

    private int getValueForChar(Character character) {
        for (CriptoPair criptoPair : encriptionAlphabet) {
            if (criptoPair.character.equals(character)) {
                return criptoPair.value;
            }
        }
        return -1;
    }


    private Character getCharForValue(int value) {
        for (CriptoPair criptoPair : encriptionAlphabet) {
            if (criptoPair.value.equals(value)) {
                return criptoPair.character;
            }
        }
        return null;
    }


    public void fromCriptotextToPlaintext() {
        for (Character character : criptotext.toCharArray()) {
            this.plaintext += getCharForValue((getValueForChar(character) - key + 26) % 26);
        }
    }

    public void printPlainText() {
        System.out.println(plaintext);
    }


    public static void main(String[] args) {
        Decryption decryption = new Decryption("/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/criptoText.txt", 2);
        decryption.fromCriptotextToPlaintext();
        decryption.printPlainText();
    }
}
