package org.elena.decryption;

import org.elena.encription.CriptoPair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class CryptoTextUtilities {
    CryptoTextUtilities instance;
    public String criptoText;
    Integer cryptoTextSize;
    public List<Digram> digramsFrequencyList;
    public List<Trigram> trigramsFrequencyList;
    public List<FrequencyCharPair> frequencyCharacterList;
    public List<CriptoPair> englishAlphabetFrequencyList;

    CryptoTextUtilities() {
        digramsFrequencyList = new ArrayList<>();
        trigramsFrequencyList = new ArrayList<>();
        frequencyCharacterList = new ArrayList<>();
        englishAlphabetFrequencyList = new ArrayList<>();
    }

    private CryptoTextUtilities getCryptoTextUtilitiesInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new CryptoTextUtilities();
        }
        return instance;
    }

    public void getCriptoTextFromFile(String input) {
        File file = new File(input);
        try {
            Scanner inputScanner = new Scanner(file);
            criptoText = inputScanner.next();
            while (inputScanner.hasNext()) {
                criptoText = criptoText + inputScanner.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        cryptoTextSize = criptoText.length();
    }

    public void setEnglishAlphabetFrequencyList(String filePath) {
        File file = new File(filePath);
        try {
            int k = 97;
            Scanner scanner = new Scanner(file);
            for (int i = 0; i < 26; i++) {
                CriptoPair criptoPair = new CriptoPair();
                criptoPair.character = (char) k;
                criptoPair.value = i;
                k++;
                criptoPair.frequency = scanner.nextDouble();
                this.englishAlphabetFrequencyList.add(criptoPair);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCharacters() {
        for (Character character = 'a'; character <= 'z'; character++) {
            FrequencyCharPair newCharPair = new FrequencyCharPair();
            newCharPair.character = character;
            newCharPair.numberOfAppearances = 0;
            frequencyCharacterList.add(newCharPair);
        }
    }

    private void incrementNumberOfAppearancesForCharacter(Character character) {
        for (FrequencyCharPair newFrequencyCharPair : frequencyCharacterList) {
            if (newFrequencyCharPair.character.equals(character))
                newFrequencyCharPair.numberOfAppearances++;
        }
    }

    private boolean isTrigramInserted(String trigramString) {
        for (Trigram trigram : trigramsFrequencyList) {
            if (trigram.getTrigramString().equals(trigramString))
                return true;
        }
        return false;
    }

    private boolean isDigramInserted(String digramString) {
        for (Digram digram : digramsFrequencyList) {
            if (digram.getDigramString().equals(digramString)) {
                return true;
            }
        }
        return false;
    }

    private void countAllCharactersOcuurence() {
        for (Character character : criptoText.toCharArray()) {
            incrementNumberOfAppearancesForCharacter(character);
        }
    }

    private void setCharactersFrequency() {
        for (FrequencyCharPair charPair : frequencyCharacterList) {
            charPair.Frequency = (double) charPair.numberOfAppearances / cryptoTextSize;
        }
    }

    private void addDigrams() {
        for (int i = 0; i < cryptoTextSize - 1; i++) {
            String digramString = criptoText.substring(i, i + 2);
            if (this.isDigramInserted(digramString) == false) {
                Digram digram = new Digram();
                digram.setDigramString(criptoText.substring(i, i + 2));
                digram.setNumberOfAppearances(0);
                digramsFrequencyList.add(digram);
            } else {
                incrementDigramsAppearanceNumber(digramString);
            }
        }
    }

    private void addTrigrams() {
        for (int i = 0; i < cryptoTextSize - 2; i++) {
            String trigramString = criptoText.substring(i, i + 3);
            if (isTrigramInserted(trigramString) == false) {
                Trigram trigram = new Trigram();
                trigram.setTrigramString(criptoText.substring(i, i + 3));
                trigram.setNumberOfAppearances(0);
                trigramsFrequencyList.add(trigram);
            } else {
                incrementTrigramsAppearanceNumber(trigramString);
            }
        }
    }

    private void incrementDigramsAppearanceNumber(String digramString) {
        for (Digram digram : digramsFrequencyList) {
            if (digram.getDigramString().equals(digramString)) {
                digram.setNumberOfAppearances(digram.getNumberOfAppearances() + 1);
            }
        }
    }

    private void incrementTrigramsAppearanceNumber(String trigramString) {
        for (Trigram trigram : trigramsFrequencyList) {
            if (trigram.getTrigramString().equals(trigramString)) {
                trigram.setNumberOfAppearances(trigram.getNumberOfAppearances() + 1);
            }
        }
    }

    private void setDigramsNumberOfOccurences() {
        for (int i = 0; i < cryptoTextSize - 1; i++) {
            String digram = criptoText.substring(i, i + 2);
            incrementDigramsAppearanceNumber(digram);
        }
    }

    private void setTrigramsNumberOfOccurences() {
        for (int i = 0; i < cryptoTextSize - 2; i++) {
            String trigram = criptoText.substring(i, i + 3);
            incrementTrigramsAppearanceNumber(trigram);
        }
    }

    private void setDigramsFrequency() {
        for (Digram digram : digramsFrequencyList
                ) {
            digram.setFrequency((float) digram.getNumberOfAppearances() / (float) digramsFrequencyList.size());
        }
    }

    private void setTrigramsFrequency() {
        for (Trigram trigram : trigramsFrequencyList
                ) {
            trigram.setFrequency(trigram.getNumberOfAppearances() / (float) digramsFrequencyList.size());
        }
    }

    public void generateDecryptionUtilities(String file) {
        getCriptoTextFromFile(file);
        setCharacters();
        countAllCharactersOcuurence();
        setCharactersFrequency();
        addDigrams();
        addTrigrams();
        setDigramsNumberOfOccurences();
        setTrigramsNumberOfOccurences();
        setDigramsFrequency();
        setTrigramsFrequency();
    }

    public void printTrigrams() {
        System.out.println(trigramsFrequencyList.size());
        for (Trigram trigram : trigramsFrequencyList) {
            System.out.print(trigram.getTrigramString());
            System.out.print(" ");
            System.out.print(trigram.getNumberOfAppearances());
            System.out.print(" ");
            System.out.printf("%.5f", trigram.getFrequency());
            System.out.println();

        }
    }

    public static void main(String[] args) {
        CryptoTextUtilities cryptoTextUtilities = new CryptoTextUtilities();
        cryptoTextUtilities.generateDecryptionUtilities("/home/elena/Faculty/sem2/ic/cryptography/src/main/java/org/elena/decryption/criptoText.txt");
        //cryptoTextUtilities.printTrigrams();
        cryptoTextUtilities.setEnglishAlphabetFrequencyList("/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/frequency");
    }
}
