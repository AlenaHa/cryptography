package org.elena.decryption;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class FrequencyCharPair {
    Character character;
    Integer numberOfAppearances;
    double Frequency;
}
