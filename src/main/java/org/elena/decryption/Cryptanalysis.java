package org.elena.decryption;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 3/9/17.
 */
public class Cryptanalysis {
    private List<CryptanalysisPairs> cryptanalysisPairsList;

    Cryptanalysis() {
        cryptanalysisPairsList = new ArrayList<>(26);
    }

    public void getStatisticsFromFilesToList(String charFileName, String bigramsFileName, String trigramsFileName) {
        File charFile = new File(charFileName);
        File bigramFile = new File(bigramsFileName);
        File trigramFile = new File(trigramsFileName);

        try {
            Scanner charScanner = new Scanner(charFile);
            Scanner bigramScanner = new Scanner(bigramFile);
            Scanner trigramScanner = new Scanner(trigramFile);


            for (int i = 0; i < 26; i++) {

                CryptanalysisPairs cryptanalysisPair;

                cryptanalysisPair = new CryptanalysisPairs(charScanner.next().charAt(0)
                        , bigramScanner.next().substring(0, 2), trigramScanner.next().substring(0, 3));

                cryptanalysisPairsList.add(cryptanalysisPair);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void main(String[] args) {
        Cryptanalysis cryptanalysisTest = new Cryptanalysis();
        cryptanalysisTest.getStatisticsFromFilesToList(
                "/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/frequency",
                "/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/bigrams",
                "/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/trigrams"
        );
    }
}
