package org.elena.encription;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class Encription {
    private int key;
    private List<CriptoPair> encriptionAlphabet = new ArrayList<>(26);
    private String criptoText;
    public String plainText;
    List<CriptoPair> criptoPairs;

    private Encription(int keyValue) {
        criptoText = "";
        key = keyValue;
        int charValue = 0;
        for (Character character = 'a'; character <= 'z'; character++) {
            CriptoPair criptoPair = new CriptoPair();
            criptoPair.character = character;
            criptoPair.value = charValue;
            encriptionAlphabet.add(criptoPair);
            charValue++;
        }
    }

    private void getPlainTextFromFile(String file) {
        File inputFile = new File(file);

        try {
            Scanner scanFile = new Scanner(inputFile);
            while (scanFile.hasNext()) {
                plainText = plainText + scanFile.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private int getValueForChar(Character character) {
        for (CriptoPair criptoPair : encriptionAlphabet) {
            if (criptoPair.character.equals(character)) {
                return criptoPair.value;
            }
        }
        return -1;
    }

    private Character getCharForValue(int value) {
        for (CriptoPair criptoPair : encriptionAlphabet) {
            if (criptoPair.value.equals(value)) {
                return criptoPair.character;
            }
        }
        return null;
    }

    private void editPlainText() {
        plainText = plainText.toLowerCase();
        plainText = plainText.replaceAll("[.,?!]", "");
        plainText = plainText.substring(4, plainText.length());

        criptoPairs = new ArrayList<>(plainText.length());

        System.out.println(plainText);
    }

    public void plainToCriptoText() {

        for (Character character : plainText.toCharArray()) {
            CriptoPair newCriptoPair = new CriptoPair();
            newCriptoPair.value = (this.getValueForChar(character) + key) % 26;
            newCriptoPair.character = this.getCharForValue(newCriptoPair.value);
            criptoText += newCriptoPair.character;
            criptoPairs.add(newCriptoPair);
        }
    }

    public void addCriptoTextToFile(String criptoFile) {

        try (FileWriter fw = new FileWriter(criptoFile, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter file = new PrintWriter(bw)) {
            file.println(criptoText);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printCriptoText() {
        for (CriptoPair criptoPair : criptoPairs) {
            System.out.print(criptoPair.character);
        }
    }

    public static void main(String[] args) {
        Encription encription = new Encription(2);
        encription.getPlainTextFromFile("/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/plainText.txt");
        encription.editPlainText();
        encription.plainToCriptoText();
        encription.addCriptoTextToFile("/home/elena/Faculty/sem2/ic/cryptography/src/main/resources/criptoText.txt");
        System.out.println(encription.criptoText);
    }

}
