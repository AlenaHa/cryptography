package org.elena.encription;

/**
 * @author Elena Hardon
 * @date 2/27/17.
 */
public class CriptoPair {
    public Character character;
    public Integer value;
    public Double frequency;
}
